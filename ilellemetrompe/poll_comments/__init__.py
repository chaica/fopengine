from django.contrib.comments.models import Comment
from poll_comments.forms import PollCommentForm

def get_model():
    return Comment

def get_form():
    return PollCommentForm
