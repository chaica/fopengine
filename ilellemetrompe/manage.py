#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os, sys

if __name__ == "__main__":
    sys.path.append('/home/chaica/progra/python/ilellemetrompe')
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ilellemetrompe.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
