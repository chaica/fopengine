//
//allow the user to vote
//
function vote(id,value){div='vote';ending="Votre avis a été enregistré, merci !";
if (!$.cookie(id)){
$('span#'+div+id).css('display','none');$('#'+div+id).html(ending).fadeIn();
$.post("/polls/" + id + "/voteyesno/",{"id":id,"value":value});
}
else {
ending="Vous avez déjà voté, désolé !";
$('span#'+div+id).css('display','none');$('#'+div+id).html(ending).fadeIn();
}
}
//
//allow the user to vote if a comment is good or not 
//
function commentvote(id,idpoll,value){div='commentvote';ending="Votre avis a été enregistré, merci !";
if (!$.cookie(idpoll+'-'+id)){
$('span#'+div+id).css('display','none');$('#'+div+id).html(ending).fadeIn();
$.post("/polls/votegoodbad/",{"id":id,"idpoll":idpoll,"value":value});
}
else {
ending="Vous avez déjà voté, désolé !";
$('span#'+div+id).css('display','none');$('#'+div+id).html(ending).fadeIn();
}
}
//
//show or hide a zone
//
function showhide(value){$('#' + value).toggle('fast');}
//
//submit a story and print the fact it has been submitted
//
function sendstory(){div='newstorybutton';ending="Votre histoire a été enregistrée, merci ! Elle sera affichée sur le site après avoir été relue par nos modérateurs.";
$('span#newstorybutton').css('display','none');$('#newstorybutton').html(ending).fadeIn();
$.post("/polls/newstory/",{"creator":$('#creator').val(),"creatorgender":$('#creatorgender').val(),"mategender":$('#mategender').val(),"story":$('#story').val(),"creatoremail":$('#creatoremail').val()});}
//
//submit a comment and print the fact it has been submitted
//
function sendcomment(){div='newcommentbutton';ending="Votre commentaire a été enregistré, merci !";
$('span#newcommentbutton').css('display','none');$('#newcommentbutton').html(ending).fadeIn();
$.post("/polls/newcomment/",{"commentcreator":$('#commentcreator').val(),"pollid":$('#pollid').val(),"comment":$('#comment').val(),"commentcreatoremail":$('#commentcreatoremail').val()});}
//
//submit an email and print the fact it has been registered
//
function sendemail(){div='getemailbutton';ending="E-mail bien enregistré, merci !";
$('span#getemailbutton').css('display','none');$('#getemailbutton').html(ending).fadeIn();
$.post("/polls/getemail/",{"email":$('#email').val()});}
//
//submit a friend address and print the fact it has been submitted
//
function sendstorytosomeone(idstory){div='newemailnotificationbutton'+idstory;ending="La notification a bien été envoyée, merci !";
$('span#newemailnotificationbutton'+idstory).css('display','none');$('#newemailnotificationbutton'+idstory).html(ending).fadeIn();
$.post("/polls/getstoryemailrecipient/",{"emailsender":$('#emailsender'+idstory).val(),"pollidforemail":$('#pollidforemail'+idstory).val(),"emailrecipient":$('#emailrecipient'+idstory).val()});}
