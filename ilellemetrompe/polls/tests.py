from django.utils import unittest
from django.test.client import Client

from polls.models import Poll
from polls.models import PollComment

class StoryFunctionalTestCase(unittest.TestCase):
    '''Test the creation of a new story'''

    def test_new_story_post(self):
        '''Test the POST and return value for a new story'''
        c = Client()
        response = c.post('/polls/newstory/', {'creator': 'Lolita', 'creatorgender': 'f', 'mategender': 'm', 'story':'this is a story test'})
        self.assertEqual(204, response.status_code)

class VoteFunctionalTestCase(unittest.TestCase):
    '''Test if a new vote is effective'''

    def setUp(self):
        '''Setup the correct database'''
        self.p = Poll.objects.create(creator='Lolita', creator_gender = 'f', mate_gender = 'm', body = 'this is a story test')

    def test_new_vote(self):
        '''Test the POST and return value for a new vote'''
        c = Client()
        response = c.post('/polls/%s/voteyesno/' % str(self.p.id), {'id': str(self.p.id), 'value': 'yes'})
        self.assertEqual(204, response.status_code)

class CommentFunctionalTestCase(VoteFunctionalTestCase):
    '''Test the creation of a new comment'''

    def test_new_comment(self):
        '''Test the POST and return value for a new comment'''
        c = Client()
        response = c.post('/polls/newcomment/', {'commentcreator': 'Marie', 'pollid': str(self.p.id), 'comment':'this is a comment test'})
        self.assertEqual(204, response.status_code)

class CommentVoteFunctionalTestCase(unittest.TestCase):
    '''Test if a vote on a comment is effective'''

    def setUp(self):
        '''Setup the correct database'''
        p = Poll.objects.create(creator='Lolita', creator_gender = 'f', mate_gender = 'm', body = 'this is a story test')
        self.pc = PollComment.objects.create(creator='Lucie', poll=p, body='this is a comment test')

    def test_new_vote(self):
        '''Test the POST and return value for a new comment vote'''
        c = Client()
        response = c.post('/polls/newcomment/', {'commentcreator': 'Marie', 'pollid': self.pc.id, 'comment':'this is a comment test'})
        self.assertEqual(204, response.status_code)
