# Create your views here.
from django.template import Context, loader
from django.shortcuts import render_to_response,  get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.core.mail import send_mail
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from polls.models import Poll
from polls.models import PollComment
from polls.models import Email
from polls.models import EmailStoryRecipient

from polls.mail import Mail
from polls.mail import CommentMail
from polls.mail import EmailStoryForAFriend

from zinnia.feeds import LatestEntries

def index(request):

    # retrieve all stories by publication date
    view_latest_poll_list = Poll.objects.all().order_by('-pub_date')
    # retrieve last 3 stories with images
    latest_image_stories = Poll.objects.exclude(image=u'').exclude(published=False).order_by('-pub_date')[:3]
    paginator = Paginator(view_latest_poll_list, 20, orphans=20) # Show 20 contacts per page

    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    # If page request (9999) is out of range, deliver last page of results.
    try:
        latest_poll_list = paginator.page(page)
    except (EmptyPage, InvalidPage):
        latest_poll_list = paginator.page(paginator.num_pages)
    return render_to_response('polls/index.html', {'latest_poll_list': latest_poll_list, 'latest_image_stories': latest_image_stories}, context_instance=RequestContext(request))

def detail(request, poll_id):
    p = get_object_or_404(Poll, pk=poll_id)
    # rerieve all the comments of the poll
    pollcomment_list = PollComment.objects.filter(poll=poll_id).filter(moderated=True).order_by('pub_date')
    # retrieve the last third stories related to the current story
    samestories_list = Poll.objects.filter(tag=p.tag).exclude(id=poll_id).order_by('pub_date')[:3]
    # retrieve last 3 stories with images
    latest_image_stories = Poll.objects.exclude(image=u'').exclude(published=False).order_by('-pub_date')[:3]
    # render the result
    return render_to_response('polls/detail.html', {'poll': p, 'pollcomment_list': pollcomment_list,'samestories_list' :  samestories_list, 'latest_image_stories': latest_image_stories},
                               context_instance=RequestContext(request))

@csrf_exempt
def voteyesno(request, poll_id):
    p = get_object_or_404(Poll, pk=poll_id)
    try:
        selected_choice = request.POST['value']
    except (KeyError):
        # Redisplay the poll voting form.
        return render_to_response('polls/detail.html', {
            'poll': p,
            'error_message': "You didn't select a choice.",
        }, context_instance=RequestContext(request))
    else:
        if selected_choice == "yes":
            p.voteyes += 1
        if selected_choice == "no":
            p.voteno += 1
        p.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        voteyesnohttpresponse = HttpResponse(status=204)
        # Create a cookie to identify later if a user has voted already
        #voteyesnohttpresponse.set_cookie(str(poll_id), poll_id, max_age=604800)
        voteyesnohttpresponse.set_cookie(str(poll_id), poll_id, max_age=60)
        return voteyesnohttpresponse

@csrf_exempt
def votegoodbad(request):
    pollcomment_id= request.POST['id']
    p = get_object_or_404(PollComment, pk=pollcomment_id)
    try:
        poll_id= request.POST['idpoll']
        selected_choice = request.POST['value']
    except (KeyError):
        # Redisplay the poll voting form.
        return render_to_response('polls/detail.html', {
            'pollcomment': p,
            'error_message': "You didn't select a choice.",
        }, context_instance=RequestContext(request))
    else:
        if selected_choice == "good":
            p.score += 1
        if selected_choice == "bad":
            p.score -= 1
        p.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        votegoodbadhttpresponse = HttpResponse(status=204)
        # Create a cookie to identify later if a user has voted already
        cookievalue = str(poll_id)+'-'+str(pollcomment_id)
        votegoodbadhttpresponse.set_cookie(cookievalue,cookievalue, max_age=60)
        return votegoodbadhttpresponse

@csrf_exempt
def newstory(request):
    creator = request.POST['creator']
    creatorgender = request.POST['creatorgender']
    mategender = request.POST['mategender']
    story = request.POST['story']
    creatoremail = request.POST['creatoremail']
    p = Poll.objects.create(creator=creator, creator_gender = creatorgender, mate_gender = mategender, body = story,creator_email = creatoremail)
    # warn moderators by email
    newstorymailbody = ("Creator:%s\n" % creator,
                        "Creatorgender:%s\n" % creatorgender,
                        "Mategender:%s\n\n" % mategender,
                        story)
    Mail(newstorymailbody)
    
    # Always return an HttpResponseRedirect after successfully dealing
    # with POST data. This prevents data from being posted twice if a
    # user hits the Back button.
    return HttpResponse(status=204)

@csrf_exempt
def newcomment(request):
    creator = request.POST['commentcreator']
    comment = request.POST['comment']
    commentcreatoremail = request.POST['commentcreatoremail']
    pollid = request.POST['pollid']
    pid = Poll.objects.get(id=pollid)
    pid.nbcomments += 1
    pid.save()
    if not commentcreatoremail:
        p = PollComment.objects.create(creator = creator, body = comment, poll = pid)
    else:
        p = PollComment.objects.create(creator = creator, body = comment, creator_email = commentcreatoremail, poll = pid)
    # warn moderator by email
    newcommentmailbody = ("Creator:%s\n" % creator,
                        comment)
    CommentMail(newcommentmailbody)
    
    # Always return an HttpResponseRedirect after successfully dealing
    # with POST data. This prevents data from being posted twice if a
    # user hits the Back button.
    return HttpResponse(status=204)

@csrf_exempt
def getemail(request):
    newemail= request.POST['email']
    email = Email.objects.create(address = newemail)
    email.save()
    # Always return an HttpResponseRedirect after successfully dealing
    # with POST data. This prevents data from being posted twice if a
    # user hits the Back button.
    return HttpResponse(status=204)

@csrf_exempt
def getstoryemailrecipient(request):
    newemail= request.POST['emailrecipient']
    sender = request.POST['emailsender']
    pollid = request.POST['pollidforemail']
    # if no sender has been defined, provide one
    if not sender:
        sender = "Quelqu'un"
    # if no email has been defined, just return a ok status (should be improved someday)
    if newemail:
        emailstoryrecipient, foundobject = EmailStoryRecipient.objects.get_or_create(address = newemail)
        if not foundobject:
            emailstoryrecipient.save()
        EmailStoryForAFriend(sender, newemail, pollid)
    # Always return an HttpResponseRedirect after successfully dealing
    # with POST data. This prevents data from being posted twice if a
    # user hits the Back button.
    return HttpResponse(status=204)
