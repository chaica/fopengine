# -*- coding: utf-8 -*-
from django.contrib import admin
from django.db import models
from polls.models import Poll
from polls.models import Tag
from polls.models import PollComment
from polls.mail import SendNewStoryEmails
from polls.mail import SendNewCommentEmail
from polls.mail import SendNewCommentEmails
from polls.foptwitter import SendNewTweet

class TagAdmin(admin.ModelAdmin):
    list_display = ('tag',)
admin.site.register(Tag, TagAdmin)

class PollAdmin(admin.ModelAdmin):
    raw_id_fields = ("tag",)
    def save_model(self, request, obj, form, change):
        # send notifications for the new story
        if obj.moderated and not obj.sentemails:
            SendNewStoryEmails(obj.id)
            obj.sentemails = True
        # send twitter notifications
        if obj.moderated and obj.twitter and not obj.published:
            tweetprefix = u'Nouvelle histoire: '
            tweetsuffix = u' http://fideleoupas.net/polls/%s #infidélité #tromper #infidèle' % obj.id
            newtweet = u'%s%s%s' % (tweetprefix, obj.twitter, tweetsuffix)
            if len(newtweet) < 140:
                SendNewTweet(newtweet)
        if obj.moderated and not obj.published:
            obj.published = True
        obj.save()
    list_display = ('id', 'pub_date','creator','voteyes','voteno')
admin.site.register(Poll, PollAdmin)

class PollCommentAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        if obj.moderated and not obj.sentnewcommentemail:
            # send the email to the creator of the poll
            pid = Poll.objects.get(id=obj.poll.id)
            # increment the number of comments
            pid.nbcomments += 1
            pid.save()
            if pid.creator_email:
                SendNewCommentEmail(pid.id, pid.creator_email)
            # send the emails to the followers of the poll
            SendNewCommentEmails(pid.id, [i.creator_email for i in PollComment.objects.filter(poll=obj.poll.id) if i.creator_email and i.creator_email != obj.creator_email])
            obj.sentnewcommentemail = True
        obj.save()

    list_display = ('pub_date','creator')
admin.site.register(PollComment, PollCommentAdmin)


