from django.db import models
import datetime

# Create your models here.

GENDER_CHOICES = (
    (u'm', u'Male'),
    (u'f', u'Female'),
)

class Tag(models.Model):
    '''Tags for poll'''
    tag = models.CharField(max_length=200, primary_key=True)

class Poll(models.Model):
    '''Components of a poll'''
    creator = models.CharField(max_length=200)
    creator_gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    mate_gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    body = models.TextField()
    pub_date = models.DateTimeField(auto_now_add=True)
    voteyes = models.PositiveIntegerField(default=0)
    voteno = models.PositiveIntegerField(default=0)
    nbcomments = models.PositiveIntegerField(default=0)
    moderated = models.BooleanField(default=False)
    sentemails = models.BooleanField(default=False)
    creator_email = models.EmailField(null=True, blank=True)
    tag = models.ForeignKey(Tag, blank=True, null=True)
    twitter = models.CharField(blank=True, max_length=140)
    published = models.BooleanField(default=False)
    image = models.CharField(blank=True, max_length=1024)
    tweet1 = models.CharField(blank=True, max_length=140)
    tweet2 = models.CharField(blank=True, max_length=140)


    @property
    def is_old(self):
        '''identify if a poll is old or not'''
        twodaysago = datetime.datetime.today().date() - datetime.timedelta(days=2)
        return twodaysago > self.pub_date.date()

class PollComment(models.Model):
    '''Components of a poll comment'''
    creator = models.CharField(max_length=200)
    body = models.TextField()
    pub_date = models.DateTimeField(auto_now_add=True)
    moderated = models.BooleanField(default=True)
    score = models.IntegerField(default=3)
    sentnewcommentemail = models.BooleanField(default=False) 
    creator_email = models.EmailField(null=True, blank=True)
    poll = models.ForeignKey(Poll)

class Email(models.Model):
    '''Components of an email'''
    address = models.EmailField(primary_key=True)

class EmailStoryRecipient(models.Model):
    '''Recipient of a story sent by email'''
    address = models.EmailField(primary_key=True)
