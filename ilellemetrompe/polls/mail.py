# -*- coding: utf-8 -*-
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from polls.models import Email
import time

###########################################################################
# Class to warn the administrators that a new message needs to be moderated
###########################################################################
class Mail:
    def __init__(self, newstorymailbody):
        send_mail('Fideleoupas.net: A new message to be moderated', ''.join(newstorymailbody), 'chaica@ohmytux.com', ['chaica@ohmytux.com'], fail_silently=False)

###########################################################################
# Class to warn the administrators that a new comment needs to be moderated
###########################################################################
class CommentMail:
    def __init__(self, newcommentmailbody):
        send_mail('Fideleoupas.net: A new comment was posted', ''.join(newcommentmailbody), 'chaica@ohmytux.com', ['chaica@ohmytux.com'], fail_silently=False)

##########################################################################
# Class to send an email to warn a subscriber that a new poll is available
##########################################################################
class SendNewStoryEmails:
    def __init__(self, pollid):
        subject, from_email = 'Nouvelle histoire sur Fideleoupas.net', 'noreply@fideleoupas.net'
        mailbody = u'Bonjour,\n\nUne nouvelle histoire[1] vous attend sur fideleoupas.net[2].\n\n[1] : http://fideleoupas.net/polls/%s\n\n[2] : http://fideleoupas.net\n\nÀ bientôt,\nLes administrateurs de Fideleoupas.net' % pollid
        text_content = mailbody
        html_content = u'<p><strong>Nouvelle histoire sur Fideleoupas.net</strong></p><p><a href="http://fideleoupas.net/polls/%s">Une nouvelle histoire</a> vous attend sur <a href="http://fideleoupas.net/">Fideleoupas.net</a>.</p><p>À bientôt,<br />Les administrateurs de Fideleoupas.net' % pollid
        for email in Email.objects.all():        
            msg = EmailMultiAlternatives(subject, text_content, from_email, [email.address])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            time.sleep(1)

##################################################################################
# Class to allow a sender to warn a friend that a new poll is available to be read
##################################################################################
class EmailStoryForAFriend:
    def __init__(self, sender, address, pollid):
        subject, from_email = '%s partage avec vous cette histoire de Fideleoupas.net' % sender, 'noreply@fideleoupas.net'
        mailbody = u'Bonjour,\n\n%s a souhaité partager avec vous cette histoire[1] disponible sur fideleoupas.net[2].\n\n[1] : http://fideleoupas.net/polls/%s\n\n[2] : http://fideleoupas.net\n\nÀ bientôt,\nLes administrateurs de Fideleoupas.net' % (sender, pollid)
        text_content = mailbody
        html_content = u'<p><strong>%s a souhaité partager avec vous <a href="http://fideleoupas.net/polls/%s">cette histoire</a> disponible sur <a href="http://fideleoupas.net/">Fideleoupas.net</a></strong></p><p>À bientôt,<br />Les administrateurs de Fideleoupas.net' % (sender, pollid)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [address])
        msg.attach_alternative(html_content, "text/html")
        msg.send()

##########################################################################################
# Class to send an email to warn the creator of the poll that a new comment is available
##########################################################################################
class SendNewCommentEmail:
    '''Send an email to warn the creator of the poll that a new comment is available'''
    def __init__(self, pollid, creatoremail):
        '''Send an email to warn the creator of the poll that a new comment is available'''
        subject, from_email = 'Nouveau commentaire sur Fideleoupas.net', 'noreply@fideleoupas.net'
        mailbody = u'Bonjour,\n\nUn nouveau commentaire[1] vous attend sur fideleoupas.net[2].\n\n[1] : http://fideleoupas.net/polls/%s\n\n[2] : http://fideleoupas.net\n\nÀ bientôt,\nLes administrateurs de Fideleoupas.net' % pollid
        text_content = mailbody
        html_content = u'<p><strong>Nouveau commentaire sur Fideleoupas.net</strong></p><p><a href="http://fideleoupas.net/polls/%s">Un nouveau commentaire</a> vous attend sur <a href="http://fideleoupas.net/">Fideleoupas.net</a>.</p><p>À bientôt,<br />Les administrateurs de Fideleoupas.net' % pollid
        msg = EmailMultiAlternatives(subject, text_content, from_email, [creatoremail])
        msg.attach_alternative(html_content, "text/html")
        msg.send()

##########################################################################################
# Class to send an email to warn the followers of the poll that a new comment is available
##########################################################################################
class SendNewCommentEmails:
    '''Send an email to warn the followers of the poll that a new comment is available'''
    def __init__(self, pollid, addresses):
        '''Send an email to warn the followers of the poll that a new comment is available'''
        subject, from_email = 'Nouveau commentaire sur Fideleoupas.net', 'noreply@fideleoupas.net'
        mailbody = u'Bonjour,\n\nUn nouveau commentaire[1] vous attend sur fideleoupas.net[2].\n\n[1] : http://fideleoupas.net/polls/%s\n\n[2] : http://fideleoupas.net\n\nÀ bientôt,\nLes administrateurs de Fideleoupas.net' % pollid
        text_content = mailbody
        html_content = u'<p><strong>Nouveau commentaire sur Fideleoupas.net</strong></p><p><a href="http://fideleoupas.net/polls/%s">Un nouveau commentaire</a> vous attend sur <a href="http://fideleoupas.net/">Fideleoupas.net</a>.</p><p>À bientôt,<br />Les administrateurs de Fideleoupas.net' % pollid
        for address in addresses:
            msg = EmailMultiAlternatives(subject, text_content, from_email, [address])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            time.sleep(1)
