import datetime
from haystack.indexes import *
from haystack import site
from polls.models import Poll


class PollIndex(SearchIndex):
    text = CharField(document=True, use_template=True)
    author = CharField(model_attr='creator')
    pub_date = DateTimeField(model_attr='pub_date')

    def index_queryset(self):
        "Used when the entire index for model is updated."
        return Poll.objects.filter(pub_date__lte=datetime.datetime.now())

site.register(Poll, PollIndex)
