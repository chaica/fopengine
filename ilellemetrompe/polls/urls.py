from django.conf.urls.defaults import *
from haystack.forms import SearchForm
from haystack.views import SearchView

urlpatterns = patterns('polls.views',

    (r'^$', 'index'),
    (r'^(?P<poll_id>\d+)/$', 'detail'),
    (r'^(?P<poll_id>\d+)/voteyesno/$', 'voteyesno'),
    (r'^votegoodbad/$', 'votegoodbad'),
    (r'^newstory/$', 'newstory'),
    (r'^newcomment/$', 'newcomment'),
    (r'^getemail/$', 'getemail'),
    (r'^getstoryemailrecipient/$', 'getstoryemailrecipient'),
    (r'^search/', SearchView(form_class=SearchForm)),
)
