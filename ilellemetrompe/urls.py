from django.conf.urls.defaults import *
from django.views.generic import TemplateView
from ilellemetrompe.feeds import RssLatestStoriesFeed,AtomLatestStoriesFeed
from ilellemetrompe.feeds import RssLatestCommentsFeed,AtomLatestCommentsFeed

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    (r'^', include('polls.urls')),
    (r'^polls/', include('polls.urls')),
    # Uncomment the next line to enable the admin:
    (r'^admin/', include(admin.site.urls)),
    (r'^weblog/', include('zinnia.urls')),
    (r'^liens/', TemplateView.as_view(template_name="liens.html")),
    (r'^comments/', include('django.contrib.comments.urls')),
    (r'^feed/histoires/rss', RssLatestStoriesFeed()),
    (r'^feed/histoires/atom', AtomLatestStoriesFeed()),
    (r'^feed/commentaires/rss', RssLatestCommentsFeed()),
    (r'^feed/commentaires/atom', AtomLatestCommentsFeed()),
)
