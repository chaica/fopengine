# -*- coding: utf-8 -*-
from django.contrib.syndication.views import Feed
from polls.models import Poll, PollComment
from django.utils.feedgenerator import Atom1Feed

class RssLatestStoriesFeed(Feed):
    title = "Flux RSS de fideleoupas.net"
    link = '/feed/histoires/rss/'
    description = u"Dernières histoires de fideleoupas.net"

    def items(self):
        pollslist = [i for i in Poll.objects.order_by('-pub_date') if i.moderated]
        return pollslist[:10]

    def item_title(self, item):
        return 'Nouvelle histoire par %s sur fideleoupas.net' % item.creator

    def item_description(self, item):
        return item.body

    def item_link(self, item):
        return '/polls/%s' % str(item.id)

class AtomLatestStoriesFeed(RssLatestStoriesFeed):
    feed_type = Atom1Feed
    subtitle = RssLatestStoriesFeed.description
    link = '/feed/histoires/atom/'

class RssLatestCommentsFeed(Feed):
    title = "Flux RSS de fideleoupas.net"
    link = '/feed/commentaires/rss/'
    description = u"Derniers commentaires de fideleoupas.net"

    def items(self):
        pollcommentslist =  [i for i in PollComment.objects.order_by('-pub_date') if i.moderated]
        return pollcommentslist[:10]

    def item_title(self, item):
        return 'Nouveau commentaire par %s sur fideleoupas.net' % item.creator

    def item_description(self, item):
        return item.body

    def item_link(self, item):
        return '/polls/%s' % str(item.poll.id)

class AtomLatestCommentsFeed(RssLatestCommentsFeed):
    feed_type = Atom1Feed
    subtitle = RssLatestCommentsFeed.description
    link = '/feed/histoires/atom/'
